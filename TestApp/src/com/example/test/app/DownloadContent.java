package com.example.test.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;

public class DownloadContent extends IntentService {

	public DownloadContent() {
		super("DownloadContent");
	}


	private final String STORAGE_PATH = Environment.getExternalStorageDirectory() + "/flipbook/";
	private final String[] FILE_NAMES = {"tnt_01.png",
			"tnt_02.png", "tnt_03.png", "tnt_04.png",
			"tnt_05.png", "tnt_06.png", "tnt_07.png",
			"tnt_08.png", "tnt_09.png"};
	private static int i = 0;
	

	@Override
	protected void onHandleIntent(Intent intent) {
		String workIntent = intent.getDataString();
		try{
			URL url = new URL(workIntent);
			HttpURLConnection mconnect = (HttpURLConnection) url.openConnection();
			mconnect.setRequestMethod("GET");
			mconnect.connect();
			
			File file = new File(STORAGE_PATH);
			if(!file.exists())
				file.mkdir();
			File outputFile = new File(STORAGE_PATH + FILE_NAMES[i]);
			if(outputFile.createNewFile()){
				FileOutputStream output = new FileOutputStream(outputFile);
				InputStream input = mconnect.getInputStream();
				byte[] buffSize = new byte[mconnect.getContentLength()];
				int data = 0;
				while((data = input.read(buffSize)) > 0){
					output.write(buffSize, 0, data);
				}
				mconnect.disconnect();
				input.close();
				output.flush();
				output.close();
			}
			if(i <= FILE_NAMES.length)
				i = 0;
			else
				i++;
		}catch(Exception ex){
			
		}
		
	}
	
}