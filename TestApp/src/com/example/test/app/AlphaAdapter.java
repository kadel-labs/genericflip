package com.example.test.app;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AlphaAdapter extends BaseAdapter
{
	private List<String> alphabets;
	
	public AlphaAdapter()
	{
		alphabets = new ArrayList<String>();
		for (int i = 65; i < 91; i++)
			alphabets.add(Character.toString((char)i));
	}
	
	@Override
	public int getCount() {
		return 26;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView tv;
		if (convertView == null)
		{
			tv = new TextView(parent.getContext());
			tv.setText(alphabets.get(position));
			tv.setTextSize(128);
			tv.setTextColor(Color.BLACK);
			tv.setBackgroundColor(Color.WHITE);
			tv.setGravity(Gravity.CENTER);
		}
		else
		{
			tv = (TextView) convertView;
			tv.setText(alphabets.get(position));
		}
		return tv;
	}

}
