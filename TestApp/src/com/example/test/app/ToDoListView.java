package com.example.test.app;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

public class ToDoListView extends TextView{

	private Paint marginPaint;
	private Paint linePaint ;
	private int paperColor;
	private Float margin;

	public ToDoListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public ToDoListView(Context context) {
		super(context);
		init();
	}
	
	public ToDoListView(Context context, AttributeSet artSet) {
		super(context, artSet);
		init();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(paperColor);
		canvas.drawLine(0, 0, getMeasuredHeight(), 0, linePaint);
		canvas.drawLine(0, getMeasuredHeight(), getMeasuredWidth(), getMeasuredHeight(), linePaint);
		
		canvas.drawLine(margin, 0, margin, getMeasuredHeight(), marginPaint);
		
		canvas.save();
		canvas.translate(margin , 0);
		super.onDraw(canvas);
		canvas.restore();
	}
	
	private void init() {
		Resources myres = getResources();
		marginPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		marginPaint.setColor(myres.getColor(R.color.notepad_margin));
		linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		linePaint.setColor(myres.getColor(R.color.notepad_lines));
		
		paperColor = myres.getColor(R.color.notepad_paper);
		margin = myres.getDimension(R.dimen.notepad_margin);
	}
}
