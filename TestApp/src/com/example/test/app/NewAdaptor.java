package com.example.test.app;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;

public class NewAdaptor extends BaseAdapter{

	private List<String> urls;
	
	public NewAdaptor(){
		urls = new ArrayList<String>();
		urls.add("http://www.google.com/");
		urls.add("http://www.gmail.com/");
		urls.add("http://www.facebook.com/");
		urls.add("https://www.linkedin.com/");
	}
	
	@Override
	public int getCount() {
		return 4;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		WebView wb;
		if(convertView == null){
			wb = new WebView(parent.getContext());
			wb.loadUrl(urls.get(position));
		}else{
			wb = (WebView) convertView;
			wb.loadUrl(urls.get(position));
		}
		return wb;
	}
}