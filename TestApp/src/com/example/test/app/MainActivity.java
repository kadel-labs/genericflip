package com.example.test.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kadel.DAO.DataSourceManager;
import com.kadel.models.CategoryModel;
import com.kadel.resources.DownloadResources;
import com.kadel.sqlite.MySQLiteDataHelper;

public class MainActivity extends Activity {

	private List<String> category_name = new ArrayList<String>();
	private DataSourceManager source;
	public static Map<String, List<String>> resource = new HashMap<String, List<String>>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trial);
		setTitle(R.string.app_name);
		// download:
		 LinearLayout verticalList = (LinearLayout) findViewById(R.id.verticalList);
		 new DownloadResources().download();
		 // accessing the database:
		source = new DataSourceManager(this);
		source.open();

		List<CategoryModel> data = source.getData();

		for (int i = 0; i < data.size(); i++) {
			CategoryModel categories = data.get(i);
			category_name.add(categories.getCategory_name());
		}
		for (int i = 0; i < data.size(); i++) {
			int j = i + 1;
			List<String> res = new ArrayList<String>();
			res = source.runQuery("select * from "
							+ MySQLiteDataHelper.TABLE_NAMES[1] + " where "
							+ MySQLiteDataHelper.BOOKS_NAMES[0] + " = " + "'"
							+ j + "'");
			resource.put(category_name.get(i), res);
		}

		int finalDimens, finalMargin;

		float density = this.getResources().getDisplayMetrics().density;
		if (density < 1.5) {
			finalDimens = (int) (100 * density);
			finalMargin = (int) (10 * density);
		} else {
			finalDimens = (int) (75 * density);
			finalMargin = (int) (5 * density);
		}
		LinearLayout.LayoutParams imgLytParams = new LinearLayout.LayoutParams(
				finalDimens, finalDimens);
		imgLytParams.setMargins(finalMargin, finalMargin, finalMargin,
				finalMargin);

		for (int i = 0; i < data.size(); i++) {
			TextView tv = new TextView(this);
			tv.setBackgroundResource(R.drawable.tab);
			tv.setPadding(0, 0, 10, 0);
			tv.setText(category_name.get(i));
			tv.setTextColor(Color.GRAY);
			tv.setTextSize(19);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			LinearLayout.LayoutParams textLayout = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			textLayout.setMargins(5, 10, 0, 0);
			tv.setLayoutParams(textLayout);

			HorizontalScrollView hsv = new HorizontalScrollView(this);
			hsv.setBackgroundResource(R.drawable.strip);

			LinearLayout hLinList = new LinearLayout(this);
			hLinList.setOrientation(LinearLayout.HORIZONTAL);
			int k = 0;
			boolean flag = false;
			for (int j = 0; j < 18; j++) {
				if (j < 9) {
					k = j;
				} else if (j >= 9 && !flag) {
					k = 0;
					flag = true;
				}

				final ImageView iv = new ImageView(this);
				iv.setLayoutParams(imgLytParams);
				List<String> temp = resource.get(category_name.get(0));
				String path = Environment.getExternalStorageDirectory()
						+ "/flipbook/" + temp.get(k);
				Bitmap image = BitmapFactory.decodeFile(path);
				iv.setImageBitmap(image);
				iv.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						Intent intent = new Intent(view.getContext(),
								FlipXMLConfigActivity.class);
						startActivity(intent);
					}
				});
				if (flag) {
					k++;
				}
				hLinList.addView(iv);
			}
			hsv.addView(hLinList);
			verticalList.addView(tv);
			verticalList.addView(hsv);
		}
		verticalList.invalidate();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		source.open();
	}

	@Override
	protected void onPause() {
		super.onPause();
		source.close();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		source.close();
	}
}