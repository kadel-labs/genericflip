package com.example.test.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.aphidmobile.flip.FlipViewController;

public class FlipViewActivity extends Activity
{
	private FlipViewController flipController;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		flipController = new FlipViewController(this, FlipViewController.HORIZONTAL);
		flipController.setAdapter(new NewAdaptor());
/*		flipController.setAdapter(new BaseAdapter() {
			
			@Override
			public View getView(int position, View convertView, ViewGroup parent)
			{
				TextView tv;
				if (convertView == null)
				{
					tv = new TextView(parent.getContext());
					tv.setText("" + position);
					tv.setTextSize(128);
					tv.setTextColor(Color.BLACK);
				    tv.setBackgroundColor(Color.WHITE);
				    tv.setGravity(Gravity.CENTER);
				}
				else
				{
					tv = (TextView) convertView;
					tv.setText("" + position);
				}
				return tv;
			}
			
			@Override
			public long getItemId(int position) {
				return position;
			}
			
			@Override
			public Object getItem(int position) {
				return position;
			}
			
			@Override
			public int getCount() {
				return 10;
			}
		});
*/		setContentView(flipController);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.flip_view, menu);
		return true;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		flipController.onResume();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		flipController.onPause();
	}
}