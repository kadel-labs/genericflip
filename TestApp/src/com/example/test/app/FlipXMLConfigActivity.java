package com.example.test.app;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.aphidmobile.flip.FlipViewController;

public class FlipXMLConfigActivity extends Activity
{
	private FlipViewController flipController;
	
/*	private ArrayList<CustomImageFlip> arrCustomImageFlips = new ArrayList<CustomImageFlip>();
	
	private CustomImageFlipAdapter arrAdapterCustomImageFlip;
*/
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_flip_xmlconfig);
		setTitle("Content for FlipView");
//		float density = this.getResources().getDisplayMetrics().density;
//		int width, height;
//		if(density < 1.5) {
//			width = (int) (550 * density);
//			height =  (int) (800 * density);
//		}else{
//			width = (int) (300 * density);
//			height = (int) (450 * density);
//		}
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int defWidth = size.x;
		int defHeight = size.y;
		
		int width = (int) (0.95 * defWidth);
		int height = (int) (0.90 * defHeight);
		LayoutParams param = new LayoutParams();
		param.width = width;
		param.height = height;
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(param);
		LinearLayout layout = (LinearLayout) findViewById(R.id.ll);
		layout.setLayoutParams(params);
		
		final FlipXMLConfigActivity actInstance = this;

		ImageButton imgButton = (ImageButton) findViewById(R.id.closeButton);
		imgButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view)
			{
				actInstance.finish();
			}
		});

		flipController = (FlipViewController) findViewById(R.id.flipView);
		
		flipController.setAdapter(new BaseAdapter() {

			@Override
			public View getView(int position, View convertView, ViewGroup parent)
			{
				WebView web;
				if (convertView == null) {
					web = new WebView(flipController.getContext());
					web.setHorizontalScrollBarEnabled(false);
					web.loadUrl("file:///android_asset/desc.html");
				}
				else {
					web = (WebView) convertView;
					web.loadUrl("file:///android_asset/desc.html");
				}
				return web;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public Object getItem(int position) {
				return position;
			}

			@Override
			public int getCount() {
				return 10;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.flip_xmlconfig, menu);
		return true;
	}

}