package com.kadel.models;

public class ResourcesModel {

	private long book_id;
	private long book_category;
	private String book_title;
	
	public long getBook_id() {
		return book_id;
	}
	
	public long getBook_category() {
		return book_category;
	}
	
	public String getBook_title() {
		return book_title;
	}
	
	public void setBook_id(long book_id) {
		this.book_id = book_id;
	}
	
	public void setBook_category(long book_category) {
		this.book_category = book_category;
	}
	
	public void setBook_title(String book_title) {
		this.book_title = book_title;
	}
	
}
