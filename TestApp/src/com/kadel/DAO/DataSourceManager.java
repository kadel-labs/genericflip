package com.kadel.DAO;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kadel.models.CategoryModel;
import com.kadel.models.ResourcesModel;
import com.kadel.resources.ImageResources;
import com.kadel.sqlite.MySQLiteDataHelper;

public class DataSourceManager {

	private SQLiteDatabase database;
	private MySQLiteDataHelper helper;

	public DataSourceManager(Context context) {
		helper = new MySQLiteDataHelper(context);
	}

	public void open() {
		try{
			database = helper.getWritableDatabase();
			createTables();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void close() {
		database.close();
	}

	private boolean createTables() {
		ContentValues values = new ContentValues();
		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Chats");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);
//		database.insert(MySQLiteDataHelper.TABLE_NAMES[0], null, values);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Events");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Group Chats");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Notes");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Favourites");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Settings");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Tasks");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Keys");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.CATEGORY_NAMES[1], "Searches");
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[0], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// table 2:
		// First row:
		values.clear();
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);
//		database.insert(MySQLiteDataHelper.TABLE_NAMES[1], null, values);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);
//		database.insert(MySQLiteDataHelper.TABLE_NAMES[1], null, values);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 2:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 3:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 4:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 5:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 6:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 7:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 8:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		// row 9:
		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 1);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[0]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 2);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[1]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 3);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[2]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 4);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[3]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 5);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[4]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 6);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[5]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 7);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[6]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 8);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[7]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);

		values.put(MySQLiteDataHelper.BOOKS_NAMES[0], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[1], 9);
		values.put(MySQLiteDataHelper.BOOKS_NAMES[2],
				ImageResources.FILE_NAMES[8]);
		database.insertWithOnConflict(MySQLiteDataHelper.TABLE_NAMES[1], null, values, SQLiteDatabase.CONFLICT_IGNORE);
		return true;
	}

	public List<CategoryModel> getData() {
		Cursor cursor = database.rawQuery("select * from " + MySQLiteDataHelper.TABLE_NAMES[0] + ";", null);
		cursor.moveToFirst();
		List<CategoryModel> data = new ArrayList<CategoryModel>();
		while (!cursor.isAfterLast()) {
			CategoryModel model = cursorToData(cursor);
			data.add(model);
			cursor.moveToNext();
		}
		return data;
	}

	public List<ResourcesModel> getResources(int book_id, int book_category) {
		Cursor cursor = database.query(MySQLiteDataHelper.TABLE_NAMES[1],
				MySQLiteDataHelper.BOOKS_NAMES, null, null, null, null, null);
		cursor.moveToFirst();
		List<ResourcesModel> data = new ArrayList<ResourcesModel>();
		while (!cursor.isAfterLast()) {
			ResourcesModel model = new ResourcesModel();
			model.setBook_id(cursor.getLong(0));
			model.setBook_category(cursor.getLong(1));
			model.setBook_title(cursor.getString(2));
			data.add(model);
			cursor.moveToNext();
		}
		return data;
	}

	public CategoryModel cursorToData(Cursor cursor) {
		CategoryModel data = new CategoryModel();
		data.setCategory_id(cursor.getLong(0));
		data.setCategory_name(cursor.getString(1));
		return data;
	}

	public List<String> runQuery(String query) {
		Cursor cursor = database.rawQuery(query, null);
		cursor.moveToFirst();
		List<String> data = new ArrayList<String>();
		while (!cursor.isAfterLast()) {
			ResourcesModel model = new ResourcesModel();
			model.setBook_title(cursor.getString(2));
			data.add(model.getBook_title());
			cursor.moveToNext();
		}
		return data;
	}
}