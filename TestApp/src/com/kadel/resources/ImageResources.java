package com.kadel.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.os.Environment;

public class ImageResources extends AsyncTask<String, Void, Void> {
	public static final String[] FILE_NAMES = { "tnt_01.png", "tnt_02.png",
			"tnt_03.png", "tnt_04.png", "tnt_05.png", "tnt_06.png",
			"tnt_07.png", "tnt_08.png", "tnt_09.png" };

	private String storage_path = Environment.getExternalStorageDirectory()
			+ "/flipbook/";
	

	@Override
	protected Void doInBackground(String... path) {

		int i = 0;
		try {
			while (i < path.length) {
				URL url = new URL(path[i]);
				HttpURLConnection connection = (HttpURLConnection) url
						.openConnection();
				connection.setRequestMethod("GET");
				connection.setDoOutput(true);
				connection.connect();

				File dir = new File(storage_path);
				boolean doesDirExist = dir.exists();
				if (!doesDirExist)
					dir.mkdir();
				File outFile = new File(dir, FILE_NAMES[i]);
				if (outFile.createNewFile()) {
					FileOutputStream out = new FileOutputStream(outFile);
					InputStream in = connection.getInputStream();
					byte[] buff = new byte[1024];
					int len = 0;
					while ((len = in.read(buff)) > 0) {
						out.write(buff, 0, len);
					}
					connection.disconnect();
					in.close();
					out.flush();
					out.close();
				} else
					continue;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}