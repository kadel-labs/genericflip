package com.kadel.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.example.test.app.DownloadContent;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

public class DownloadResources {

	private final String STORAGE_PATH = Environment
			.getExternalStorageDirectory() + "/flipbook/";

	public DownloadResources() {
		super();
	}

	private final String[] FILE_NAMES = { "tnt_01.png", "tnt_02.png",
			"tnt_03.png", "tnt_04.png", "tnt_05.png", "tnt_06.png",
			"tnt_07.png", "tnt_08.png", "tnt_09.png" };

	private String[] path = {
			"http://electricityservice.hostei.com/tnt_icon_01.png",
			"http://electricityservice.hostei.com/tnt_icon_02.png",
			"http://electricityservice.hostei.com/tnt_icon_06.png",
			"http://electricityservice.hostei.com/tnt_icon_07.png",
			"http://electricityservice.hostei.com/tnt_icon_08.png",
			"http://electricityservice.hostei.com/tnt_icon_12.png",
			"http://electricityservice.hostei.com/tnt_icon_13.png",
			"http://electricityservice.hostei.com/tnt_icon_14.png",
			"http://electricityservice.hostei.com/tnt_icon_15.png" };

	public void downloadImagesToSdcard(Context context) {
		Intent mintent = new Intent(context, DownloadContent.class);
		for (int i = 0; i < path.length; i++) {
			mintent.setData(Uri.parse(path[i]));
			context.startService(mintent);
		}
	}

	public void download() {
		new Runnable() {
			@Override
			public void run() {
				int i = 0;
				while (i < FILE_NAMES.length) {
					try {
						URL url = new URL(FILE_NAMES[i]);
						HttpURLConnection mconnect = (HttpURLConnection) url
								.openConnection();
						mconnect.setRequestMethod("GET");
						mconnect.connect();

						File file = new File(STORAGE_PATH);
						if (!file.exists())
							file.mkdir();
						File outputFile = new File(STORAGE_PATH + FILE_NAMES[i]);
						if (outputFile.createNewFile()) {
							FileOutputStream output = new FileOutputStream(outputFile);
							InputStream input = mconnect.getInputStream();
							byte[] buffSize = new byte[mconnect.getContentLength()];
							int data = 0;
							while ((data = input.read(buffSize)) > 0) {
								output.write(buffSize, 0, data);
							}
							mconnect.disconnect();
							input.close();
							output.flush();
							output.close();
						}
					} catch (Exception ex) {

					}
				}

			}
		};
	}
}