package com.kadel.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteDataHelper extends SQLiteOpenHelper {

	private final static String DATABASE_NAME = "flipbook.db";
	private final static int DATABASE_VERSION = 1;
	public final static String[] TABLE_NAMES = { "category_master", "books" };
	public final static String[] CATEGORY_NAMES = {"category_id", "category_name" };
	public final static String[] BOOKS_NAMES = {"book_id", "book_category", "book_title"};

	// query..
	private static final String CREATE_TABLE_1 = "create table if not exists category_master(category_id integer primary key autoincrement, category_name text unique not null);";
	private static final String CREATE_TABLE_2 = "create table if not exists books(book_id integer , book_category integer, book_title text unique not null, primary key(book_id, book_category));";
//	private static final String CREATE_TABLE_3 = "create table book_details(book_id integer primary key autoincrement, book_pages integer not null, book_src text not null);";

	public MySQLiteDataHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_1);
		db.execSQL(CREATE_TABLE_2);
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//updating the database version..
		Log.w(MySQLiteDataHelper.class.getName(),
				"Upgrading Database version from " + oldVersion + "to "
						+ newVersion
						+ "which destroys all old data and creates new tables");
		db.execSQL("DROP TABLES IF EXISTS" + TABLE_NAMES[0]);
		db.execSQL("DROP TABLES IF EXISTS" + TABLE_NAMES[1]);
		onCreate(db);
	}

}