package com.genericflip.async.exception;

public class OperationException extends GenericException {

   public OperationException() {
      super();
   }

   public OperationException(int errorCode) {
      super();
   }

   public OperationException(String errorMessage) {
      super(errorMessage);
   }

   public OperationException(int errorCode, String errorMessage) {
      super(errorMessage);
   }

   public OperationException(int errorCode, String errorMessage, Throwable th) {
      super(errorCode, errorMessage, th);
   }

   public static class DateFromStringException extends OperationException {
   }
}