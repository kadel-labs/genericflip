package com.genericflip.async.exception;

/**
 * 
 * @author Bhoopender Pal Singh
 * @copyright SproutObjects Technologies Pvt. Ltd.
 */
public class InvalidParameterException extends GenericException {

   public InvalidParameterException() {
      super();
   }

   public InvalidParameterException(int errorCode) {
      super();
   }

   public InvalidParameterException(String errorMessage) {
      super(errorMessage);
   }

   public InvalidParameterException(int errorCode, String errorMessage) {
      super(errorMessage);
   }

   public InvalidParameterException(int errorCode, String errorMessage, Throwable th) {
      super(errorCode, errorMessage, th);
   }
}