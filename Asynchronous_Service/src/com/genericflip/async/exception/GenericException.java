package com.genericflip.async.exception;

/**
 * 
 * @author Bhoopender Pal Singh
 * @copyright SproutObjects Technologies Pvt. Ltd.
 */
public class GenericException extends Exception {

   protected static final long serialVersionUID = -1L;
   protected int errorCode = -1;
   protected String errorMessage = "Some error has occured. We are notified and working on this.";

   public GenericException() {
      super();
   }

   public GenericException(int errorCode) {
      super();
      this.errorCode = errorCode;
   }

   public GenericException(String errorMessage) {
      super(errorMessage);
      this.errorMessage = errorMessage;
   }

   public GenericException(int errorCode, String errorMessage) {
      super(errorMessage);
      this.errorCode = errorCode;
      this.errorMessage = errorMessage;
   }

   public GenericException(int errorCode, String errorMessage, Throwable th) {
      super(errorMessage, th);
      this.errorCode = errorCode;
      this.errorMessage = errorMessage;
   }

   public int getErrorCode() {
      return errorCode;
   }

   public void setErrorCode(int errorCode) {
      this.errorCode = errorCode;
   }

   public String getErrorMessage() {
      return errorMessage;
   }

   public void setErrorMessage(String errorMessage) {
      this.errorMessage = errorMessage;
   }
}