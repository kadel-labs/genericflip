package com.genericflip.async;

public interface IServerConfiguration {

   public String getServerUrl();
}
