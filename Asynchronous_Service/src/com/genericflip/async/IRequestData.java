package com.genericflip.async;

import com.genericflip.async.impl.RequestData;



public interface IRequestData {
   public static final String METHOD_NAME = "POST";

   public RequestData getRequestData();
}
