package com.genericflip.async;

import android.net.Uri;

public interface IAsynchronousService {

	public long POLLING_INTERVAL = 3 * 60 * 1000; // milliseconds
	public static final String MAINAUTHORITY = "genericflip.databasecontentprovider";
	public static final Uri CONTENT_URI_PROPERTY = Uri.parse("content://"
			+ MAINAUTHORITY + "/LEVELCONFIG");
}
