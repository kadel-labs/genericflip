package com.genericflip.async.impl;

import org.apache.http.HttpResponse;

import com.genericflip.async.IResponseData;



public class ResponseData implements IResponseData {

   private HttpResponse response;

   public HttpResponse getResponse() {
      return response;
   }

   public void setResponse(HttpResponse response) {
      this.response = response;
   }

}
