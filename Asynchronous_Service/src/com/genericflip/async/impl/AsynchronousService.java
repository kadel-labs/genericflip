package com.genericflip.async.impl;

import java.util.concurrent.Executors;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.IBinder;

import com.genericflip.async.IAsynchronousService;
import com.genericflip.async.IRequestData;
import com.genericflip.async.IResponseData;
import com.genericflip.async.IServerConfiguration;
import com.genericflip.async.exception.InvalidParameterException;
import com.genericflip.async.util.Constant;
import com.genericflip.async.util.Utils;

public abstract class AsynchronousService extends Service implements
		IAsynchronousService {

	private final IBinder mBinder = new MyBinder();

	@Override
	public void onCreate() {
		super.onCreate();
		// Toast.makeText(getApplicationContext(), "On Create",
		// Toast.LENGTH_SHORT).show();
		System.out.println("onCreate of service*******");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

	public class MyBinder extends Binder {
		public AsynchronousService getService() {
			return AsynchronousService.this;
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Toast.makeText(getApplicationContext(), "On Start",
		// Toast.LENGTH_SHORT).show();

		System.out.println("Async service started.");

		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {

				while (true) {

					try {
						execute();
						Thread.sleep(getPollingInterval());
					} catch (Exception e) {
						e.printStackTrace();
						try {
							Thread.sleep(10000);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}
			}

		});

		return super.onStartCommand(intent, flags, startId);
	}

	protected void postData() {

		System.out.println("Post Data...");

		// get srever configuration
		ServerConfiguration serverConfiguration = (ServerConfiguration) getServerConfiguration();

		// get request data
		RequestData requestData = (RequestData) getRequestData();

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = null;

			// POST method
			if (requestData.getMethodName().equals(Constant.HTTP_POST_METHOD)) {
				HttpPost httppost = new HttpPost(
						serverConfiguration.getServerUrl());
				httppost.setEntity(new UrlEncodedFormEntity(requestData
						.getNameValuePairs()));
				httppost.addHeader("Content-type",
						"application/x-www-form-urlencoded");
				response = httpclient.execute(httppost);

				// GET method
			} else if (requestData.getMethodName().equals(
					Constant.HTTP_GET_METHOD)) {

				// other methods
			} else {
				throw new InvalidParameterException("Method Not Supported: "
						+ requestData.getMethodName());
			}

			// handle response

			ResponseData responseData = new ResponseData();
			responseData.setResponse(response);
			handleServerResponse(responseData);

		} catch (Exception e) {
			// Toast.makeText(SplashActivity.this, "Exception",
			// Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	protected long getPollingInterval() {
		return this.POLLING_INTERVAL;
	}

	/**
	 * 
	 * @param get
	 *            Property value by passing property key
	 * @return
	 */
	protected String getPropertyValue(String propertyKey) {
		Cursor cursorTable4 = getApplicationContext().getContentResolver()
				.query(CONTENT_URI_PROPERTY, null, null, null, null);
		String propertyValue = null;
		while (cursorTable4.moveToNext()) {

			if ((cursorTable4.getString(cursorTable4
					.getColumnIndex("key_field")).equalsIgnoreCase(propertyKey))) {
				propertyValue = cursorTable4.getString(cursorTable4
						.getColumnIndex("value_field"));
			}
		}
		if (propertyValue == null || propertyValue.isEmpty()) {
			return "";
		} else {
			return propertyValue;
		}
	}

	protected abstract IServerConfiguration getServerConfiguration();

	protected abstract IRequestData getRequestData();

	protected abstract void handleServerResponse(IResponseData responseData);

	protected abstract void execute();

	protected String getMacAddress() {
		return Utils.getMacAddress(getApplicationContext());
	}
}
