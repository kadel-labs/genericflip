package com.genericflip.async.impl;

import com.genericflip.async.IServerConfiguration;



public class ServerConfiguration implements IServerConfiguration {

   private String serverUrl;

   public ServerConfiguration(String serverUrl) {
      this.serverUrl = serverUrl;
   }

   @Override
   public String getServerUrl() {
      return serverUrl;
   }
}
