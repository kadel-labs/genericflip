package com.genericflip.async.impl;

import java.util.List;

import org.apache.http.NameValuePair;

import com.genericflip.async.IRequestData;



public class RequestData implements IRequestData {

   private List<NameValuePair> nameValuePairs;
   private String queryString;
   private String methodName;

   @Override
   public RequestData getRequestData() {
      return this;
   }

   public List<NameValuePair> getNameValuePairs() {
      return nameValuePairs;
   }

   public void setNameValuePairs(List<NameValuePair> nameValuePairs) {
      this.nameValuePairs = nameValuePairs;
   }

   public String getQueryString() {
      return queryString;
   }

   public void setQueryString(String queryString) {
      this.queryString = queryString;
   }

   public String getMethodName() {
      return methodName;
   }

   public void setMethodName(String methodName) {
      this.methodName = methodName;
   }

}
