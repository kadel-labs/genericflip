package com.genericflip.async.jobs;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.ContentValues;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;

import com.genericflip.async.IRequestData;
import com.genericflip.async.IResponseData;
import com.genericflip.async.IServerConfiguration;
import com.genericflip.async.asynchronousservice.AsyncMainActivity;
import com.genericflip.async.impl.AsynchronousService;
import com.genericflip.async.impl.RequestData;
import com.genericflip.async.impl.ServerConfiguration;
import com.genericflip.async.util.Constant;
import com.genericflip.async.util.Utils;

public class PropertyValueService extends AsynchronousService {

   private static final String AUTHORITY = "kafappv2.databasecontentprovider";
   private static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
	 + "/TABLE4");
   // private static final String SERVER_URL =
   // "https://creator.zoho.com/api/json/test-buddydubai-by-allcad/view/TEST_PropertyData_View?authtoken=52cee4f07639ac7676a79650ce435d3c&raw=true&scope=creatorapi&criteria=(modification_date_time%3E%2201-Jan-2013%2012:46:43%22)";
   private static String UrlToPropertyValueServerlUrl = null;
   private static String prefixToPropertyValueServerlUrl = null;
   private static String PropertyValueServerlUrl = null;
   private RequestData requestData;
   private ServerConfiguration serverConfiguration;

   private String hotelID;

   private String jsonValueString;
   private static JSONObject jsonObject = null;
   private static boolean servicesStarted;
   private String macAddress = null;

   @Override
   public void onCreate() {
      // TODO Auto-generated method stub

      super.onCreate();
      getUrlToPropertyValueURL();
      macAddress=Utils.getMacAddress(getApplicationContext());
      //macAddress = "14:F4:2A:54:72:04";
      UrlToPropertyValueServerlUrl = UrlToPropertyValueServerlUrl + "%22"
	    + macAddress + "%22)";
      System.out.println("UrlToPropertyValueServerlUrl in oncreate"+UrlToPropertyValueServerlUrl);
   }

   @Override
   protected void execute() {
      System.out.println("exceute() of PropertyAsyncService....");

      try {

	 /*
	  * Parse Json Object from UrlToPropertyValueServerlUrl to get Prefix of
	  * PropertyValueServerlUrlParse Json Object from
	  * UrlToPropertyValueServerlUrl to get Prefix of
	  * PropertyValueServerlUrl
	  */
	 JSONObject json1 = getJSONFromUrl(UrlToPropertyValueServerlUrl,
	       Constant.HTTP_GET_METHOD);
	 JSONArray jsonArray1 = json1.getJSONArray("BDSDLC");
	 JSONObject propertyValueObj1 = jsonArray1.getJSONObject(0);
	 prefixToPropertyValueServerlUrl = propertyValueObj1
	       .getString("ENVIRONMENT_URL");
/*	 PropertyValueServerlUrl = prefixToPropertyValueServerlUrl
	       + "?authtoken=52cee4f07639ac7676a79650ce435d3c&raw=true&scope=creatorapi&criteria=(modification_date_time%3E%2201-Jan-2013%2012:46:43%22)";
*/	 
	 PropertyValueServerlUrl = prefixToPropertyValueServerlUrl
	       + "&criteria=(modification_date_time%3E%2201-Jan-2013%2012:46:43%22)";
	 
	 System.out.println("PropertyValueServerlUrl------"
	       + PropertyValueServerlUrl + "UrlToPropertyValueServerlUrl----"
	       + UrlToPropertyValueServerlUrl
	       + "prefixToPropertyValueServerlUrl---"
	       + prefixToPropertyValueServerlUrl);
	 
	 /**
	  * if url for propertyValues is null then close the app
	  */
	 if (PropertyValueServerlUrl == null) {
	    // Logger.printLogAndEmail("Property URL is null at server so app is existing.",
	    // LogTypeEnum.ERROR);
	    System.exit(0);
	 }

	 /*
	  * Parse Json Object from PropertyValueServerlUrl to values of key and
	  * value
	  */

	 JSONObject json = getJSONFromUrl(PropertyValueServerlUrl,
	       Constant.HTTP_GET_METHOD);
	 JSONArray jsonArray = json.getJSONArray("TEST_PropertyData");
	 for (int i = 0; i < jsonArray.length(); i++) {
	    JSONObject propertyValueObj = jsonArray.getJSONObject(i);
	    String creation_date_time = propertyValueObj
		  .getString("creation_date_time");
	    String isactive = propertyValueObj.getString("isactive");
	    String group = propertyValueObj.getString("group");
	    String modification_date_time = propertyValueObj
		  .getString("modification_date_time");
	    String modifier_id = propertyValueObj.getString("modifier_id");
	    String creator_id = propertyValueObj.getString("creator_id");

	    String key = propertyValueObj.getString("key");
	    String value = propertyValueObj.getString("value");

	    ContentValues propertyData = new ContentValues();
	    propertyData.put("creation_date_time", creation_date_time);
	    propertyData.put("isactive", isactive);
	    propertyData.put("groups", group);
	    propertyData.put("modification_date_time", modification_date_time);
	    propertyData.put("modifier_id", modifier_id);
	    propertyData.put("creator_id", creator_id);
	    propertyData.put("key_field", key);
	    propertyData.put("value_field", value);

	    Cursor cursor = getApplicationContext().getContentResolver().query(
		  CONTENT_URI, null, " key_field=? ", new String[] { key },
		  null);

	    // update
	    if (cursor.moveToNext()) {

	       // if modified
	       String localDate = cursor.getString(cursor
		     .getColumnIndex("modification_date_time"));
	       if (localDate != null
		     && !localDate.isEmpty()
		     && Utils.getDateFromString(modification_date_time)
		           .getTime() > Utils.getDateFromString(localDate)
		           .getTime()) {
		  getApplicationContext().getContentResolver().update(
			CONTENT_URI, propertyData, " key_field=? ",
			new String[] { key });
	       }

	       // insert
	    } else {
	       getApplicationContext().getContentResolver().insert(CONTENT_URI,
		     propertyData);
	    }

	    /*
	     * if (getApplicationContext().getContentResolver().update(
	     * Uri.parse("content://" + AUTHORITY + "/TABLE4/key/" + key),
	     * propertyData, null, null) != 0) {
	     * System.out.println("update called");
	     * getApplicationContext().getContentResolver().update(
	     * Uri.parse("content://" + AUTHORITY + "/TABLE4/key/" + key),
	     * propertyData, null, null); } else {
	     * System.out.println("insert called");
	     * getApplicationContext().getContentResolver().insert( CONTENT_URI,
	     * propertyData); }
	     */

	 }
	 handleServerResponse(null);

      } catch (InvalidParameterException e) {
	 // TODO Auto-generated catch block
	 e.printStackTrace();
      } catch (IOException e) {
	 // TODO Auto-generated catch block
	 e.printStackTrace();
      } catch (JSONException e) {
	 // TODO Auto-generated catch block
	 e.printStackTrace();
      } catch (Exception e) {
	 e.printStackTrace();
      }

   }

   private void getUrlToPropertyValueURL() {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = null;
      try {
	 builder = factory.newDocumentBuilder();
      } catch (ParserConfigurationException e1) {
	 e1.printStackTrace();
      }
      AssetManager assetManager = getAssets();
      InputStream source = null;
      try {
	 source = assetManager.open("data.xml");
      } catch (IOException e) {
	 // TODO Auto-generated catch block
	 e.printStackTrace();
      }
      String url = "file:///android_asset/data.xml";
      // File source = new File(url);
      Document dom = null;
      try {
	 dom = builder.parse(source);
      } catch (SAXException e1) {
	 // TODO Auto-generated catch block
	 e1.printStackTrace();
      } catch (IOException e1) {
	 // TODO Auto-generated catch block
	 e1.printStackTrace();
      }

      Element root = dom.getDocumentElement();

      NodeList propertyValue = root.getElementsByTagName("propertyValue");
      Element propertyValueURL = (Element) propertyValue.item(0);
      UrlToPropertyValueServerlUrl = propertyValueURL.getAttribute("url");
      System.out.println("SERVER_URL---------" + UrlToPropertyValueServerlUrl);
   }

   private JSONObject getJSONFromUrl(String url, String httpGetMethod)
	 throws InvalidParameterException, IOException, JSONException {

      try {

	 DefaultHttpClient httpClient = new DefaultHttpClient();

	 HttpRequestBase httpRequestBase = null;

	 if (httpGetMethod == Constant.HTTP_GET_METHOD) {
	    httpRequestBase = new HttpGet(url);
	 } else if (httpGetMethod == Constant.HTTP_POST_METHOD) {

	    httpRequestBase = new HttpPost(url);

	    if (getJson() != null) {
	       StringEntity se = new StringEntity(getJson());
	       ((HttpPost) httpRequestBase).setEntity(se);
	    }
	 } else {
	    // System.out.println("---json exception 000");
	    throw new InvalidParameterException("Method Not Supported");
	 }

	 httpRequestBase.setHeader("Content-type", "application/json");
	 HttpResponse httpResponse = httpClient.execute(httpRequestBase);
	 jsonValueString = EntityUtils.toString(httpResponse.getEntity());

	 // System.out.println("----before json");
	 jsonObject = new JSONObject(jsonValueString);
	 /*
	  * System.out.println("-JSONParser--jsonObject---/" +
	  * jsonObject.toString());
	  */

	 return jsonObject;

      } catch (UnsupportedEncodingException e) {
	 System.out.println("--UnsupportedEncodingException---/");
	 e.printStackTrace();
	 throw new UnsupportedEncodingException("");
      } catch (ClientProtocolException e) {
	 System.out.println("--ClientProtocolException---/");
	 e.printStackTrace();
	 throw new ClientProtocolException("");
      } catch (IOException e) {
	 System.out.println("--IOException---/");
	 // e.printStackTrace();
	 throw new IOException("Services Not Working");
      } catch (JSONException e) {
	 System.out.println("--JSONException---/");
	 // e.printStackTrace();
	 throw new JSONException("JSON Exception");
      }

   }

   private String getJson() {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   protected long getPollingInterval() {
      System.out.println("ajit ---"+getPropertyValue("polling_interval_for_propertyvalue_service"));
      return Long
	    .parseLong(getPropertyValue("polling_interval_for_propertyvalue_service"));
   }

   @Override
   protected IServerConfiguration getServerConfiguration() {
      serverConfiguration = new ServerConfiguration(PropertyValueServerlUrl);
      return serverConfiguration;
   }

   @Override
   protected IRequestData getRequestData() {
      return requestData;
   }

   @Override
   protected void handleServerResponse(IResponseData responseData) {

      if (servicesStarted) {
	 return;
      }

      AsyncMainActivity.startDataDependentServices(getApplicationContext());
      servicesStarted = true;

   }
}
