package com.genericflip.async.util;

public class Constant {
   public static final String HTTP_GET_METHOD = "GET";
   public static final String HTTP_POST_METHOD = "POST";
}
