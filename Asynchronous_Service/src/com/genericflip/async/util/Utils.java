package com.genericflip.async.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.genericflip.async.exception.OperationException;



public class Utils {

   /**
    * 
    * @param dateStrValue
    *           : 12-Jan-2013 07:33:02
    * @param dateStrFormat
    *           : dd-MMM-yyyy hh:mm:ss
    * @return Date
    */
   public static Date getDateFromString(String dateStrValue, String dateStrFormat)
         throws OperationException.DateFromStringException {

      if (dateStrFormat == null || dateStrFormat.trim().isEmpty()) {
         dateStrFormat = "dd-MMM-yyyy hh:mm:ss";
      }

      SimpleDateFormat dateFormat = new SimpleDateFormat(dateStrFormat);
      Date date = null;

      try {
         date = dateFormat.parse(dateStrValue);
      } catch (Exception e) {
         throw new OperationException.DateFromStringException();
      }
      return date;
   }

   public static Date getDateFromString(String dateStrValue) throws OperationException.DateFromStringException {
      return getDateFromString(dateStrValue, null);
   }

   /**
    * 
    * @param date
    * @param dateStrFormat
    *           : dd-MMM-yyyy hh:mm:ss
    * @return
    * @throws OperationException.DateFromStringException
    */
   public static String getStringFromDate(Date date, String dateStrFormat)
         throws OperationException.DateFromStringException {

      if (dateStrFormat == null || dateStrFormat.trim().isEmpty()) {
         dateStrFormat = "dd-MMM-yyyy hh:mm:ss";
      }

      SimpleDateFormat dateFormat = new SimpleDateFormat(dateStrFormat);
      String dateStr = null;

      try {
         dateStr = dateFormat.format(date);
      } catch (Exception e) {
         throw new OperationException.DateFromStringException();
      }
      return dateStr;
   }

   public static String getStringFromDate(Date date) throws OperationException.DateFromStringException {
      return getStringFromDate(date, null);
   }

   public static String encodeUrl(String url) throws UnsupportedEncodingException {
      return URLEncoder.encode(url, "utf-8");
   }

   public static String getMacAddress(Context context) {

      WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
      WifiInfo wInfo = wifiManager.getConnectionInfo();
      String macAddress = wInfo.getMacAddress();
      //String macAddress = "14:F4:2A:54:72:04";// "Mac-3434";
      System.out.println("Mac Address" + macAddress);
      return macAddress;
   }
}
