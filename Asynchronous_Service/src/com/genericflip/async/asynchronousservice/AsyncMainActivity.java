package com.genericflip.async.asynchronousservice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.buddydubai.tabhire.async.R;
import com.genericflip.async.jobs.PropertyValueService;

public class AsyncMainActivity extends Activity {

   private static long SLEEP_TIME = 5;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      /*
       * IntentLauncher launcher = new IntentLauncher();
       * 
       * launcher.start();
       */
      startService();

   }

   private class IntentLauncher extends Thread {

      @Override
      /**
       * Sleep for some time and than start new activity.
       */
      public void run() {
         try {
            // Sleeping
            Thread.sleep(SLEEP_TIME * 1000);
         } catch (Exception e) {
            e.printStackTrace();
         }

         // start aysnc service
         startService();
      }
   }

   private void startService() {

      // Start PropertyValueService
      Intent propertyValueService = new Intent(this, PropertyValueService.class);
      startService(propertyValueService);

   }

   public static void startDataDependentServices(Context context) {
      // Start Service1

      // start Service2

      // start Service3
   }
}
