package com.genericflip.async.asynchronousservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.genericflip.async.jobs.PropertyValueService;



public class MyStartServiceReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		// Start PropertyValueService
		Intent propertyValueService = new Intent(context,
				PropertyValueService.class);
		context.startService(propertyValueService);
	
	

		
	}
}